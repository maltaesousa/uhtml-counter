# µhtml + Vite with html templated files

This is a simple component written in typescript, bundled with Vite and using µhtml library.

# Getting started

```
npm i
npm start
```

# File structure

```
📦root
 ┣ 📂buildtools         // contains two scripts for vite allowing html file templating
 ┣ 📂src
 ┃ ┣ 📂components       // contains your components
 ┃ ┣ 📜index.html       // where your componenents are used
 ┃ ┗ 📜main.ts          // where your componenents are imported and defined
 ┣ 📜.gitignore
 ┣ 📜package-lock.json
 ┗ 📜package.json
```
