import { render } from 'uhtml';
import { Hole } from 'uhtml/keyed';

class MyCounter extends HTMLElement {
  template?: () => Hole;
  count: number;
  templateUrl = './template.html';
  styleUrl = './style.css';

  constructor() {
    super();
    this.count = 0;
    this.attachShadow({ mode: 'open' });
  }

  connectedCallback() {
    this.update();
  }

  inc = () => {
    this.count++;
    this.update();
  };

  dec = () => {
    this.count--;
    this.update();
  };

  update() {
    render(this.shadowRoot, this.template!);
  }
}

export default MyCounter;
